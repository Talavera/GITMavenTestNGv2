import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.OutputType;
import org.openqa.selenium.Platform;
import org.openqa.selenium.TakesScreenshot;
//import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.DriverCommand;
import org.openqa.selenium.remote.RemoteExecuteMethod;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.AfterClass;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.AfterSuite;

import ru.yandex.qatools.allure.annotations.Attachment;

import com.perfectomobile.selenium.MobileConstants;
import com.perfectomobile.selenium.util.EclipseConnector;

public class SeleniumRwdTest {
	
	public  RemoteWebDriver driver;
	public WebDriverWait wait; 
	public config myConfig;
	public String sDeviceOS;
	public boolean bEclipse=true;
	
 
  @BeforeMethod
  public void beforeMethod() {
  }

  @AfterMethod
  public void afterMethod() {
  }


//  @DataProvider
//  public Object[][] dp() {
//    return new Object[][] {
//      new Object[] { 1, "a" },
//      new Object[] { 2, "b" },
//    };
//  }
  @BeforeClass
  public void beforeClass() {
  }

  @AfterClass
  public void afterClass() {
  }

	@Parameters({ "targetEnv" })
	@BeforeTest
	public void beforeTest(String targetEnvironment) throws UnsupportedEncodingException, MalformedURLException {
		
		String browserName = "mobileOS";
		DesiredCapabilities capabilities = new DesiredCapabilities(browserName, "", Platform.ANY);
		switch (targetEnvironment) {
		case "Samsung":
			//Setting capabilities to identify device- by Description, OS, OS-Version etc.
			capabilities.setCapability("platformName", "Android");
			capabilities.setCapability("deviceName", "06157DF6C9C4D131");
			break;

		case "IPhone 6":
			capabilities.setCapability("platformName", "iOS");
			capabilities.setCapability("deviceName", "AA4B9E35795760DFBEDB8C7D4CAFCC261044ED50");
			break;
		}
		
		myConfig = new config();  
		String host = myConfig.host;   //"demo.perfectomobile.com";			
		capabilities.setCapability("user", myConfig.user);
		capabilities.setCapability("password", myConfig.password);		
	//	capabilities.setCapability("windTunnelPersona", "Sara");  //empty Ross Sara
   //     capabilities.setCapability("browserName", browserName);

		
		try {
			if (bEclipse)
				setExecutionIdCapability(capabilities);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			System.out.println("SetExecutionCapabilities Error: ");
			//e.printStackTrace();
		}
		
	

        driver = new RemoteWebDriver(new URL("https://" + host + "/nexperience/perfectomobile/wd/hub"), capabilities);

		sDeviceOS= (String) capabilities.getCapability("platformName");	
		System.out.println(sDeviceOS);
		driver.manage().timeouts().implicitlyWait(60, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(60, TimeUnit.SECONDS);

	}

	 @Test
	  public void openApp(){
		 
		 try{
			/**
			* start vitals
			*/
			
			Map<String, Object> params = new HashMap<>();
			List<String> vitals = new ArrayList<>();
			vitals.add("all");
			params.put("vitals", vitals);
			List<String> sources = new ArrayList<>();
			sources.add("device");
			params.put("sources", sources);
			Object result = driver.executeScript("mobile:monitor:start", params);
			
			sleep(3000);
			
			// write your code here
			params = new HashMap<>();
			driver.executeScript("mobile:handset:ready", params);
			
			driver.get("http://www.sap.com/fiori-demo");

			params = new HashMap<>();
			params.put("content", "Experience SAP ");
			params.put("timeout", "30");
			result= driver.executeScript("mobile:checkpoint:text", params);

			params = new HashMap<>();
			params.put("content", "See it in action");
			params.put("analysis", "automatic");
			params.put("scrolling", "scroll");
			driver.executeScript("mobile:checkpoint:text", params);

			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//A[text()=\"See it in action\"])[1]").click();

			params = new HashMap<>();
			params.put("content", "Customize and Extend");
			params.put("analysis", "automatic");
			params.put("timeout", "30");
			 driver.executeScript("mobile:checkpoint:text", params);
			
			 params = new HashMap<>();
			params.put("content", "My travel requests");
			params.put("threshold", "95");
			params.put("analysis", "automatic");
			params.put("scrolling", "scroll");
			params.put("next", "SWIPE_UP");
			driver.executeScript("mobile:checkpoint:text", params);
			
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//div[@id=\"__tile25\"])[1]").click();
			
			
			params = new HashMap<>();
			params.put("content", "Pitch a customer");
			params.put("timeout", "60");
			driver.executeScript("mobile:checkpoint:text", params);
			
			//click + sign
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//button[@id=\"__button15\"])[1]").click();
		//Select new
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//button[@id=\"__button17\"])[1]").click();

			params = new HashMap<>();
			params.put("label", "Search");
			params.put("timeout", "30");
			driver.executeScript("mobile:button-text:click", params);

			driver.getKeyboard().sendKeys("business");
			
			if (sDeviceOS.equals("Android"))
			{
				Map<String, Object> params121 = new HashMap<>();
				params121.put("keySequence", "ENTER");
				driver.executeScript("mobile:presskey", params121);				
			}
			
			params = new HashMap<>();
			params.put("label", "Business trip");
			driver.executeScript("mobile:button-text:click", params);
			
			params = new HashMap<>();
			params.put("content", "Report details");
			params.put("timeout", "60");
			result = driver.executeScript("mobile:checkpoint:text", params);
			
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//input[@id=\"__xmlview5Fragment--fromInputTime-inner\"])[1]").click();
		
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("//*[text()=\"OK\"]").click();
			
			params = new HashMap<>();
			params.put("content", "Add Expense");
			params.put("threshold", "95");
			params.put("analysis", "automatic");
			params.put("scrolling", "scroll");
			params.put("next", "Swipe=(50%,70%),(50%,10%)");
			driver.executeScript("mobile:checkpoint:text", params);

			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//span[@id=\"__xmlview5Fragment--expensesButton-content\"])[1]").click();

//			switchToContext(driver, "WEBVIEW");
//			driver.findElementByXPath("(//input[@id=\"__xmlview5Fragment--ClaimSelectDialog--ExpenseTypeSelectDialog-searchField-I\"])[1]").click();

			Map<String, Object> params8 = new HashMap<>();
			params8.put("content", "Per Diem");
			params8.put("timeout", "60");
			Object result8 = driver.executeScript("mobile:checkpoint:text", params8);
			
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("//*[text()=\"Mileage\"]").click();
			
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("//*[text()=\"OK\"]").click();
			
			Map<String, Object> params11 = new HashMap<>();
			params11.put("content", "Total Distance");
			params11.put("timeout", "60");
			Object result11 = driver.executeScript("mobile:checkpoint:text", params11);
			
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//input[@id=\"__xmlview6MileageClaimFragment--totalDistanceInput-inner\"])[1]").sendKeys("500");
			
			switchToContext(driver, "WEBVIEW");
driver.findElementByXPath("(//input[@id=\"__xmlview6MileageClaimFragment--dateInput-inner\"])[1]").click();


			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//*[text()=\"9\"])[1]").click();
			
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//input[@id=\"__xmlview6MileageClaimFragment--FromDestinationInput-inner\"])[1]").sendKeys("Frankfurt");
			
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//input[@id=\"__xmlview6MileageClaimFragment--ToDestinationInput-inner\"])[1]").sendKeys("Walldorf");
			
			switchToContext(driver, "WEBVIEW");
			driver.findElementByXPath("(//input[@id=\"__xmlview6MileageClaimFragment--countryInput-inner\"])[1]").click();
			
			Map<String, Object> params22 = new HashMap<>();
			params22.put("content", "Germany");
			params22.put("timeout", "20");
			Object result22 = driver.executeScript("mobile:checkpoint:text", params22);
	
			
			} catch (Exception e) {
				// TODO: handle exception
				e.printStackTrace();
			} finally {takeScreenshot();}
			
	  }

  @AfterTest
  public void afterTest() {
  }

  @BeforeSuite
  public void beforeSuite() {
  }

  @AfterSuite
  public void afterSuite() {
  }

	private static void sleep(long millis) {
		try {
			Thread.sleep(millis);
		} catch (InterruptedException e) {
		}
	}
	
	@Test	
	public void testTearDown() throws Exception {
		System.out.println("####### Tear Down ########");
		if (driver != null) {
			/**
			* Stop vitals
			*/
			Map<String, Object> params8 = new HashMap<>();
			Object result8 = driver.executeScript("mobile:monitor:stop", params8);

			driver.close();
			
			downloadReport("html");
			RemoteWebDriverUtils.downloadReport(driver, "html", "/Users/joset/Documents/Reports/rep_" + System.currentTimeMillis());
			//RemoteWebDriverUtils.downloadAttachment(driver, "video", "C:\\test\\report\\video\\", "flv");
			//RemoteWebDriverUtils.downloadAttachment(driver, "image", "C:\\test\\report\\images\\", "jpg");
		
			driver.quit();	
		}
	}
	
	@Attachment
	private byte[] downloadReport(String type) throws IOException
	{	
		String command = "mobile:report:download";
		Map<String, String> params = new HashMap<>();
		params.put("type", type);
		String report = (String)((RemoteWebDriver) driver).executeScript(command, params);
		byte[] reportBytes = OutputType.BYTES.convertFromBase64Png(report);
		return reportBytes;
	}
	
	private static void switchToContext(RemoteWebDriver driver, String context) {
		RemoteExecuteMethod executeMethod = new RemoteExecuteMethod(driver);
		Map<String,String> params = new HashMap<String,String>();
		params.put("name", context);
		executeMethod.execute(DriverCommand.SWITCH_TO_CONTEXT, params);
	}

	private static String getCurrentContextHandle(RemoteWebDriver driver) {		  
		RemoteExecuteMethod executeMethod = new RemoteExecuteMethod(driver);
		String context =  (String) executeMethod.execute(DriverCommand.GET_CURRENT_CONTEXT_HANDLE, null);
		return context;
	}

	private static List<String> getContextHandles(RemoteWebDriver driver) {		  
		RemoteExecuteMethod executeMethod = new RemoteExecuteMethod(driver);
		List<String> contexts =  (List<String>) executeMethod.execute(DriverCommand.GET_CONTEXT_HANDLES, null);
		return contexts;
	}
	
 	private static void setExecutionIdCapability(DesiredCapabilities capabilities) throws IOException {
		EclipseConnector connector = new EclipseConnector();
		String executionId = connector.getExecutionId();
		capabilities.setCapability(EclipseConnector.ECLIPSE_EXECUTION_ID, executionId);
	}
	private static Object setPointOfInterest (RemoteWebDriver driver, String description, String status) {
		Map<String, Object> params = new HashMap<>();
		String command = MobileConstants.MOBILE_PREFIX + MobileConstants.STATUS_OBJECT + MobileConstants.GENERIC_COMMAND_SEP + MobileConstants.EVENT_OPERATION;
		params = new HashMap<>();
		params.put(MobileConstants.DESCRIPTION_PARAM, description);
		params.put(MobileConstants.STATUS_PARAM, status);
		return driver.executeScript(command, params);
		
	}
	
	private static Object setTimerCommand (RemoteWebDriver driver, Object result, Integer threshold, String description, String status, String name ){
		String command = MobileConstants.MOBILE_PREFIX + MobileConstants.STATUS_OBJECT + MobileConstants.GENERIC_COMMAND_SEP + MobileConstants.TIMER_OPERATION;
		Map<String, Object> params = new HashMap<>();
		params.put(MobileConstants.RESULT_PARAM, result);
		params.put(MobileConstants.THRESHOLD_PARAM, threshold);
		params.put(MobileConstants.DESCRIPTION_PARAM, description);
		params.put(MobileConstants.STATUS_PARAM, status);
		params.put(MobileConstants.NAME_PARAM, name);
		return driver.executeScript(command, params);
	}
    @Attachment
    public byte[] takeScreenshot() {
        System.out.println("Taking screenshot");
        return ((TakesScreenshot) driver).getScreenshotAs(OutputType.BYTES);
    }
	
}
